import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Random;

public class GameDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonStart;
    private JButton buttonQuit;
    private JTextArea outputArea;
    private JLabel statusLabel;

    private int x = outputArea.getColumns()/2-2;
    private int y = 0;
    private char currentChar = 'O';
    private String allShapes = "IJLOSTZ";
    private char shape = 'I';
    private int orientation = 0;
    private GameThread gameThread;
    private int score = 0;

    public GameDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonStart);

        buttonStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onStart();
            }
        });

        buttonQuit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onQuit();
            }
        });

        char[] initString = new char[outputArea.getColumns()*outputArea.getRows()];
        Arrays.fill(initString, ' ');
        outputArea.append(String.valueOf(initString));
        outputArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                processKey(e);
            }
        });

        gameThread = new GameThread();
    }
    private void onStart() {
        gameThread.start();
        buttonStart.setEnabled(false);
        outputArea.requestFocus();
    }
    private void onQuit() {
        // add your code here if necessary
        dispose();
    }
    private void processKey(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                if (checkBottomBound()) {
                    printShape(' ', orientation);
                    y++;
                }
                else {
                    resetPos();
                }
                handleFullLine();
//                statusLabel.setText("DOWN");
                break;
            case KeyEvent.VK_UP:
                printShape(' ', orientation);
                shape = getNextShape();
//                statusLabel.setText(Character.toString(shape));
                break;
            case KeyEvent.VK_LEFT:
                if (x > 0) {
                    printShape(' ', orientation);
                    --x;
                }
//                statusLabel.setText("LEFT");
                break;
            case KeyEvent.VK_RIGHT:
                if (checkRightBound()) {
                    printShape(' ', orientation);
                    x++;
                }
//                statusLabel.setText("RIGHT");
                break;
            case KeyEvent.VK_SPACE:
                for (int i = y; i < outputArea.getRows(); i++) {
                    if (checkBottomBound()) {
                        printShape(' ', orientation);
                        y++;
                    }
                }
                printShape(currentChar, orientation);
                resetPos();
                handleFullLine();
//                statusLabel.setText("FALL");
                break;
            case KeyEvent.VK_Z:
                if (orientation == 1 || orientation == 3 || orientation == -1 || orientation == -3) {
                    if (shape == 'I' && x < outputArea.getColumns()-3) {
                        printShape(' ', orientation);
//                        statusLabel.setText("ROTATE LEFT");
                        orientation = (orientation + 1) % 4;
                    }
                    if (shape != 'I' && x < outputArea.getColumns()-2) {
                        printShape(' ', orientation);
//                        statusLabel.setText("ROTATE LEFT");
                        orientation = (orientation + 1) % 4;
                    }
                }
                else {
                    if (shape == 'I' && y < outputArea.getRows()-3) {
                        printShape(' ', orientation);
//                        statusLabel.setText("ROTATE LEFT");
                        orientation = (orientation + 1) % 4;
                    }
                    if (shape != 'I' && y < outputArea.getRows()-2) {
                        printShape(' ', orientation);
//                        statusLabel.setText("ROTATE LEFT");
                        orientation = (orientation + 1) % 4;
                    }
                }
                break;
            case KeyEvent.VK_X:
                if (orientation == 1 || orientation == 3 || orientation == -1 || orientation == -3) {
                    if (shape == 'I' && x < outputArea.getColumns()-3) {
                        printShape(' ', orientation);
//                        statusLabel.setText("ROTATE RIGHT");
                        orientation = (orientation - 1) % 4;
                    }
                    if (shape != 'I' && x < outputArea.getColumns()-2) {
                        printShape(' ', orientation);
//                        statusLabel.setText("ROTATE RIGHT");
                        orientation = (orientation - 1) % 4;
                    }
                }
                else {
                    if (shape == 'I' && y < outputArea.getRows()-3) {
                        printShape(' ', orientation);
//                        statusLabel.setText("ROTATE RIGHT");
                        orientation = (orientation - 1) % 4;
                    }
                    if (shape != 'I' && y < outputArea.getRows()-2) {
                        printShape(' ', orientation);
//                        statusLabel.setText("ROTATE RIGHT");
                        orientation = (orientation - 1) % 4;
                    }
                }
                break;
            default:
//                statusLabel.setText(Character.toString(e.getKeyChar()));
        }
        printShape(currentChar, orientation);

    }

    private char getChar(int x, int y) {
        try {
            return outputArea.getText(y * outputArea.getColumns() + x, 1).charAt(0);
        } catch (BadLocationException e) {
            e.printStackTrace();
            return ' ';
        }
    }
    private void setChar(char c, int x, int y) {
        int pos = y * outputArea.getColumns() + x;
        outputArea.replaceRange(Character.toString(c), pos, pos+1);
    }

    private char getNextShape() {
        Random random = new Random();
        int i = random.nextInt(allShapes.length());
        return allShapes.charAt(i);
    }

    private void resetPos() {
        shape = getNextShape();
        y = 0;
        x = outputArea.getColumns()/2 - 1;
    }

    private void handleFullLine() {
        for (int i = 0; i < outputArea.getRows(); i++) {
            boolean fullLine = Boolean.TRUE;
            for (int j = 0; j < outputArea.getColumns(); j++) {
                if (getChar(j, i) == ' ') {
                    fullLine = false;
                    break;
                }
            }
            if (fullLine) {
                score++;
                for (int j = 0; j < outputArea.getColumns(); j++) {
                    setChar(' ', j, i);
                }
                for (int j = i; j > -1; j--) {
                    for (int k = 0; k < outputArea.getColumns(); k++) {
                        if (getChar(k, j) != ' ') {
                            setChar(' ',k, j);
                            setChar(currentChar, k, j+1);
                        }
                    }
                }
            }
        }
        statusLabel.setText("SCORE: " + score);
    }

    private boolean checkBottomBound() {
        if (orientation == 0 || orientation == 2 || orientation == -2) {
            if (shape == 'I') {
                if (y < outputArea.getRows() - 1) {
                    return true;
                }
            }
            else {
                if (y < outputArea.getRows() - 2) {
                    return true;
                }
            }
        }
        else {
            if (shape == 'O') {
                if (y < outputArea.getRows() - 2) {
                    return true;
                }
            }
            if (shape == 'I') {
                if (y < outputArea.getRows() - 4) {
                    return true;
                }
            }
            else {
                if (y < outputArea.getRows() - 3) {
                    return true;
                }
            }
        }
        return false;
    }
    private boolean checkRightBound() {

        if (orientation == 0 || orientation == 2 || orientation == -2){
            if (shape == 'O') {
                if (x < outputArea.getColumns()-2) {
                    return  true;
                }
            }
            if (shape == 'I') {
                if (x < outputArea.getColumns()-4) {
                    return  true;
                }
            }
            else {
                if (x < outputArea.getColumns()-3) {
                    return  true;
                }
            }
        }
        else {
            if (shape == 'I') {
                if (x < outputArea.getColumns()-1) {
                    return true;
                }
            }
            else {
                if (x < outputArea.getColumns()-2) {
                    return true;
                }
            }
        }
        return false;
    }

    private void printShape(char printChar, int orientation) {
            switch (shape) {
            case 'I':
                printIshape(printChar, orientation);
                break;
            case  'J':
                printJshape(printChar, orientation);
                break;
            case 'L':
                printLshape(printChar, orientation);
                break;
            case 'O':
                printOshape(printChar);
                break;
            case 'S':
                printSshape(printChar, orientation);
                break;
            case 'T':
                printTshape(printChar, orientation);
                break;
            case 'Z':
                printZshape(printChar, orientation);
                break;
        }
    }

    private void printOshape (char printChar) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                setChar(printChar,x+i,y+j);
            }
        }
    }
    private void printIshape (char printChar, int orientation) {
        switch (orientation) {
            case 0:
            case 2:
            case -2:
                for (int i = 0; i < 4; i++) {
                    setChar(printChar, x + i, y);
                }
                break;
            case 1:
            case 3:
            case -1:
            case -3:
                for (int i = 0; i < 4; i++) {
                    setChar(printChar,x,y+i);
                }
                break;
        }
    }
    private void printTshape (char printChar, int orientation) {
        switch (orientation) {
            case 0:
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+i,y);
                }
                setChar(printChar,x+1,y+1);
                break;
            case 1:
            case -3:
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x,y+i);
                }
                setChar(printChar,x+1,y+1);
                break;
            case 2:
            case -2:
                setChar(printChar,x+1,y);
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+i,y+1);
                }
                break;
            case 3:
            case -1:
                setChar(printChar,x,y+1);
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+1,y+i);
                }
                break;
        }
    }
    private void printLshape (char printChar, int orientation) {
        switch (orientation) {
            case 0:
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+i,y);
                }
                setChar(printChar,x,y+1);
                break;
            case 1:
            case -3:
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x,y+i);
                }
                setChar(printChar,x+1,y+2);
                break;
            case 2:
            case -2:
                setChar(printChar,x+2,y);
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+i,y+1);
                }
                break;
            case 3:
            case -1:
                setChar(printChar,x,y);
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+1,y+i);
                }
                break;
        }
    }
    private void printJshape (char printChar, int orientation) {
        switch (orientation) {
            case 0:
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+i,y);
                }
                setChar(printChar,x+2,y+1);
                break;
            case 1:
            case -3:
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x,y+i);
                }
                setChar(printChar,x+1,y);
                break;
            case 2:
            case -2:
                setChar(printChar,x,y);
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+i,y+1);
                }
                break;
            case 3:
            case -1:
                setChar(printChar,x,y+2);
                for (int i = 0; i < 3; i++) {
                    setChar(printChar,x+1,y+i);
                }
                break;
        }
    }
    private void printSshape (char printChar, int orientation) {
        switch (orientation) {
            case 0:
            case 2:
            case -2:
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        setChar(printChar,x+1-i+j,y+i);
                    }
                }
                break;
            case 1:
            case 3:
            case -1:
            case -3:
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        setChar(printChar,x+i,y+j+i);
                    }
                }
                break;
        }
    }
    private void printZshape (char printChar, int orientation) {
        switch (orientation) {
            case 0:
            case 2:
            case -2:
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        setChar(printChar, x+i+j, y+j);
                    }
                }
                break;
            case 1:
            case 3:
            case -1:
            case -3:
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 2; j++) {
                        setChar(printChar,x+i, y+j+1-i);
                    }
                }
                break;
        }
    }

    public static void main(String[] args) {
        GameDialog dialog = new GameDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private class GameThread extends Thread {
        private int sleepTime = 1000;
        public void run() {
            while(true) {
                try {
                    sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (y == 0) {
                    printShape(currentChar, orientation);
                    try {
                        sleep(sleepTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                if (checkBottomBound()) {
                    printShape(' ', orientation);
                    y++;
                }
                else {
                    resetPos();
                }
                printShape(currentChar, orientation);
                handleFullLine();
            }
        }
    }
}


